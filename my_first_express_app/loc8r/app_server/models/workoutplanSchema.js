const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const WorkoutplanShema =  new Schema({
    name: String,
    exercises: [{type: Schema.ObjectId, ref:'Exercise'}]
});


WorkoutplanShema
.virtual('url')
.get(function(){
    return '/catalog/workoutplan/'+this._id;
});

module.exports = mongoose.model('Workoutplan', WorkoutplanShema);