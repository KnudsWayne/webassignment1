const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const ExerciseSchema =  new Schema({
    name: String,
    description: String,
    sets: Number,
    reps: String
});


module.exports = mongoose.model('Exercise', ExerciseSchema);